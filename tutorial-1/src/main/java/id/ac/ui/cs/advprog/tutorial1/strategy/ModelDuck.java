package id.ac.ui.cs.advprog.tutorial1.strategy;

public class ModelDuck extends Duck {
    public ModelDuck() {
        FlyBehavior flyBehavior = new FlyNoWay();
        QuackBehavior quackBehavior = new Quack();

        this.setFlyBehavior(flyBehavior);
        this.setQuackBehavior(quackBehavior);
    }

    @Override
    public void display() {
        System.out.println("I'm a model duck");
    }
}
