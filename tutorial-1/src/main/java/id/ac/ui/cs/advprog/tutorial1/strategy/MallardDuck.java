package id.ac.ui.cs.advprog.tutorial1.strategy;

public class MallardDuck extends Duck {
    public MallardDuck() {
        FlyBehavior flyBehavior = new FlyWithWings();
        QuackBehavior quackBehavior = new Quack();

        this.setFlyBehavior(flyBehavior);
        this.setQuackBehavior(quackBehavior);
    }

    @Override
    public void display() {
        System.out.println("I'm a real Mallard duck");
    }
}
