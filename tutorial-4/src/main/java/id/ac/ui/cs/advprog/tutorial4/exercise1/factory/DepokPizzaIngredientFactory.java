package id.ac.ui.cs.advprog.tutorial4.exercise1.factory;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.Cheese;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.DangkeCheese;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.Clams;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.PernaViridis;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.Dough;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.RegularCrustDough;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.Sauce;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.SoySauce;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Cabbage;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Veggies;

public class DepokPizzaIngredientFactory implements PizzaIngredientFactory {
    @Override
    public Dough createDough() {
        return new RegularCrustDough();
    }

    @Override
    public Sauce createSauce() {
        return new SoySauce();
    }

    @Override
    public Cheese createCheese() {
        return new DangkeCheese();
    }

    @Override
    public Veggies[] createVeggies() {
        return new Cabbage[]{new Cabbage()};
    }

    @Override
    public Clams createClam() {
        return new PernaViridis();
    }
}
