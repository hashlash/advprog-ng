package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

public class PernaViridis implements Clams {

    public String toString() {
        return "Asian famous tasty green clams";
    }
}
